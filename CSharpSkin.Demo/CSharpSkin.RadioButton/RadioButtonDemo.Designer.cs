﻿namespace CSharpSkin.Demo
{
    partial class RadioButtonDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpRadioButton2 = new CSharpSkin.RadioButton.CSharpRadioButton();
            this.cSharpRadioButton1 = new CSharpSkin.RadioButton.CSharpRadioButton();
            this.cSharpRadioButton3 = new CSharpSkin.RadioButton.CSharpRadioButton();
            this.SuspendLayout();
            // 
            // cSharpRadioButton2
            // 
            this.cSharpRadioButton2.BackColor = System.Drawing.Color.Transparent;
            this.cSharpRadioButton2.Location = new System.Drawing.Point(25, 96);
            this.cSharpRadioButton2.Name = "cSharpRadioButton2";
            this.cSharpRadioButton2.RadioColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpRadioButton2.Size = new System.Drawing.Size(187, 34);
            this.cSharpRadioButton2.TabIndex = 1;
            this.cSharpRadioButton2.TabStop = true;
            this.cSharpRadioButton2.Text = "cSharpRadioButton2";
            this.cSharpRadioButton2.UseVisualStyleBackColor = false;
            // 
            // cSharpRadioButton1
            // 
            this.cSharpRadioButton1.AutoSize = true;
            this.cSharpRadioButton1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpRadioButton1.Location = new System.Drawing.Point(25, 40);
            this.cSharpRadioButton1.Name = "cSharpRadioButton1";
            this.cSharpRadioButton1.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(134)))), ((int)(((byte)(241)))));
            this.cSharpRadioButton1.Size = new System.Drawing.Size(131, 16);
            this.cSharpRadioButton1.TabIndex = 0;
            this.cSharpRadioButton1.TabStop = true;
            this.cSharpRadioButton1.Text = "cSharpRadioButton1";
            this.cSharpRadioButton1.UseVisualStyleBackColor = false;
            // 
            // cSharpRadioButton3
            // 
            this.cSharpRadioButton3.BackColor = System.Drawing.Color.Transparent;
            this.cSharpRadioButton3.Location = new System.Drawing.Point(25, 158);
            this.cSharpRadioButton3.Name = "cSharpRadioButton3";
            this.cSharpRadioButton3.RadioColor = System.Drawing.Color.Fuchsia;
            this.cSharpRadioButton3.Size = new System.Drawing.Size(187, 34);
            this.cSharpRadioButton3.TabIndex = 2;
            this.cSharpRadioButton3.TabStop = true;
            this.cSharpRadioButton3.Text = "cSharpRadioButton3";
            this.cSharpRadioButton3.UseVisualStyleBackColor = false;
            // 
            // RadioButtonDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpRadioButton3);
            this.Controls.Add(this.cSharpRadioButton2);
            this.Controls.Add(this.cSharpRadioButton1);
            this.Name = "RadioButtonDemo";
            this.Text = "RadioButtonDemo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RadioButton.CSharpRadioButton cSharpRadioButton1;
        private RadioButton.CSharpRadioButton cSharpRadioButton2;
        private RadioButton.CSharpRadioButton cSharpRadioButton3;
    }
}