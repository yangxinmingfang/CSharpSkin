﻿namespace CSharpSkin.Demo
{
    partial class ToolTipDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.鼠标移动上去 = new System.Windows.Forms.Label();
            this.cSharpToolTip1 = new CSharpSkin.ToolTip.CSharpToolTip(this.components);
            this.SuspendLayout();
            // 
            // 鼠标移动上去
            // 
            this.鼠标移动上去.AutoSize = true;
            this.鼠标移动上去.Location = new System.Drawing.Point(28, 32);
            this.鼠标移动上去.Name = "鼠标移动上去";
            this.鼠标移动上去.Size = new System.Drawing.Size(41, 12);
            this.鼠标移动上去.TabIndex = 0;
            this.鼠标移动上去.Text = "label1";
            this.cSharpToolTip1.SetToolTip(this.鼠标移动上去, "你好吗大哥");
            // 
            // cSharpToolTip1
            // 
            this.cSharpToolTip1.AutoPopDelay = 5000;
            this.cSharpToolTip1.InitialDelay = 500;
            this.cSharpToolTip1.OwnerDraw = true;
            this.cSharpToolTip1.ReshowDelay = 800;
            this.cSharpToolTip1.ToolTipColorStyle.BackHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpToolTip1.ToolTipColorStyle.BackNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpToolTip1.ToolTipColorStyle.BackPressedColor = System.Drawing.Color.Red;
            this.cSharpToolTip1.ToolTipColorStyle.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.cSharpToolTip1.ToolTipColorStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpToolTip1.ToolTipColorStyle.TipForeColor = System.Drawing.Color.Lime;
            this.cSharpToolTip1.ToolTipColorStyle.TitleForeColor = System.Drawing.Color.White;
            this.cSharpToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.cSharpToolTip1.ToolTipTitle = "ASDasdaasd";
            // 
            // ToolTipDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.鼠标移动上去);
            this.Name = "ToolTipDemo";
            this.Text = "ToolTipDemo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label 鼠标移动上去;
        private ToolTip.CSharpToolTip cSharpToolTip1;
    }
}