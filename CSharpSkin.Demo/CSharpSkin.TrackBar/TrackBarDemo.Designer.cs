﻿namespace CSharpSkin.Demo
{
    partial class TrackBarDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpTrackBar6 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar5 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar4 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar3 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar2 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar1 = new CSharpSkin.TrackBar.CSharpTrackBar();
            this.cSharpTrackBar7 = new CSharpSkin.TrackBar.CSharpTrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar7)).BeginInit();
            this.SuspendLayout();
            // 
            // cSharpTrackBar6
            // 
            this.cSharpTrackBar6.Location = new System.Drawing.Point(32, 279);
            this.cSharpTrackBar6.Name = "cSharpTrackBar6";
            this.cSharpTrackBar6.Size = new System.Drawing.Size(293, 45);
            this.cSharpTrackBar6.TabIndex = 5;
            this.cSharpTrackBar6.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.Gray;
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.Olive;
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.Olive;
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(64)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(64)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar6.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // cSharpTrackBar5
            // 
            this.cSharpTrackBar5.Location = new System.Drawing.Point(478, 55);
            this.cSharpTrackBar5.Name = "cSharpTrackBar5";
            this.cSharpTrackBar5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cSharpTrackBar5.Size = new System.Drawing.Size(45, 285);
            this.cSharpTrackBar5.TabIndex = 4;
            this.cSharpTrackBar5.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.Maroon;
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.Teal;
            this.cSharpTrackBar5.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.Lime;
            this.cSharpTrackBar5.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar5.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            // 
            // cSharpTrackBar4
            // 
            this.cSharpTrackBar4.Location = new System.Drawing.Point(390, 55);
            this.cSharpTrackBar4.Name = "cSharpTrackBar4";
            this.cSharpTrackBar4.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cSharpTrackBar4.Size = new System.Drawing.Size(45, 285);
            this.cSharpTrackBar4.TabIndex = 3;
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(64)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.Red;
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.Red;
            this.cSharpTrackBar4.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.Red;
            this.cSharpTrackBar4.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.Red;
            this.cSharpTrackBar4.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar4.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            // 
            // cSharpTrackBar3
            // 
            this.cSharpTrackBar3.Location = new System.Drawing.Point(65, 133);
            this.cSharpTrackBar3.Name = "cSharpTrackBar3";
            this.cSharpTrackBar3.Size = new System.Drawing.Size(293, 45);
            this.cSharpTrackBar3.TabIndex = 2;
            this.cSharpTrackBar3.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(192)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(192)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar3.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // cSharpTrackBar2
            // 
            this.cSharpTrackBar2.Location = new System.Drawing.Point(48, 44);
            this.cSharpTrackBar2.Name = "cSharpTrackBar2";
            this.cSharpTrackBar2.Size = new System.Drawing.Size(293, 45);
            this.cSharpTrackBar2.TabIndex = 1;
            this.cSharpTrackBar2.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.Fuchsia;
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.Fuchsia;
            this.cSharpTrackBar2.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar2.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // cSharpTrackBar1
            // 
            this.cSharpTrackBar1.Location = new System.Drawing.Point(32, 199);
            this.cSharpTrackBar1.Name = "cSharpTrackBar1";
            this.cSharpTrackBar1.Size = new System.Drawing.Size(293, 45);
            this.cSharpTrackBar1.TabIndex = 0;
            this.cSharpTrackBar1.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(50)))), ((int)(((byte)(162)))), ((int)(((byte)(228)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(193)))), ((int)(((byte)(227)))), ((int)(((byte)(247)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(50)))), ((int)(((byte)(162)))), ((int)(((byte)(228)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(146)))), ((int)(((byte)(207)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(165)))), ((int)(((byte)(216)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar1.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // cSharpTrackBar7
            // 
            this.cSharpTrackBar7.Location = new System.Drawing.Point(584, 147);
            this.cSharpTrackBar7.Name = "cSharpTrackBar7";
            this.cSharpTrackBar7.Size = new System.Drawing.Size(104, 45);
            this.cSharpTrackBar7.TabIndex = 6;
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBackgroundHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBackgroundNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBackgroundPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBorderColor = System.Drawing.Color.Green;
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBorderHoverColor = System.Drawing.Color.Olive;
            this.cSharpTrackBar7.TrackBarColorStyle.TrackBorderNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackInnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpTrackBar7.TrackBarColorStyle.TrackLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // TrackBarDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpTrackBar7);
            this.Controls.Add(this.cSharpTrackBar6);
            this.Controls.Add(this.cSharpTrackBar5);
            this.Controls.Add(this.cSharpTrackBar4);
            this.Controls.Add(this.cSharpTrackBar3);
            this.Controls.Add(this.cSharpTrackBar2);
            this.Controls.Add(this.cSharpTrackBar1);
            this.Name = "TrackBarDemo";
            this.Text = "TrackBarDemo";
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpTrackBar7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TrackBar.CSharpTrackBar cSharpTrackBar1;
        private TrackBar.CSharpTrackBar cSharpTrackBar2;
        private TrackBar.CSharpTrackBar cSharpTrackBar3;
        private TrackBar.CSharpTrackBar cSharpTrackBar4;
        private TrackBar.CSharpTrackBar cSharpTrackBar5;
        private TrackBar.CSharpTrackBar cSharpTrackBar6;
        private TrackBar.CSharpTrackBar cSharpTrackBar7;
    }
}