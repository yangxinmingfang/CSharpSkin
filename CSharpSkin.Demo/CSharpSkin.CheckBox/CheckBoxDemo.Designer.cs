﻿namespace CSharpSkin.Demo
{
    partial class CheckBoxDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpCheckBox1 = new CSharpSkin.CheckBox.CSharpCheckBox();
            this.cSharpCheckBox2 = new CSharpSkin.CheckBox.CSharpCheckBox();
            this.cSharpCheckBox3 = new CSharpSkin.CheckBox.CSharpCheckBox();
            this.SuspendLayout();
            // 
            // cSharpCheckBox1
            // 
            this.cSharpCheckBox1.AutoSize = true;
            this.cSharpCheckBox1.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(134)))), ((int)(((byte)(241)))));
            this.cSharpCheckBox1.CheckBoxState = CSharpSkin.CheckBox.CheckBoxState.Normal;
            this.cSharpCheckBox1.Location = new System.Drawing.Point(44, 49);
            this.cSharpCheckBox1.Name = "cSharpCheckBox1";
            this.cSharpCheckBox1.Size = new System.Drawing.Size(114, 16);
            this.cSharpCheckBox1.TabIndex = 0;
            this.cSharpCheckBox1.Text = "cSharpCheckBox1";
            this.cSharpCheckBox1.UseVisualStyleBackColor = true;
            // 
            // cSharpCheckBox2
            // 
            this.cSharpCheckBox2.CheckBoxColor = System.Drawing.Color.Red;
            this.cSharpCheckBox2.CheckBoxState = CSharpSkin.CheckBox.CheckBoxState.Focused;
            this.cSharpCheckBox2.Location = new System.Drawing.Point(44, 82);
            this.cSharpCheckBox2.Name = "cSharpCheckBox2";
            this.cSharpCheckBox2.Size = new System.Drawing.Size(114, 70);
            this.cSharpCheckBox2.TabIndex = 1;
            this.cSharpCheckBox2.Text = "cSharpCheckBox2";
            this.cSharpCheckBox2.UseVisualStyleBackColor = true;
            // 
            // cSharpCheckBox3
            // 
            this.cSharpCheckBox3.AutoSize = true;
            this.cSharpCheckBox3.CheckBoxColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpCheckBox3.CheckBoxState = CSharpSkin.CheckBox.CheckBoxState.Normal;
            this.cSharpCheckBox3.Checked = true;
            this.cSharpCheckBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cSharpCheckBox3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cSharpCheckBox3.Location = new System.Drawing.Point(44, 175);
            this.cSharpCheckBox3.Name = "cSharpCheckBox3";
            this.cSharpCheckBox3.Size = new System.Drawing.Size(147, 20);
            this.cSharpCheckBox3.TabIndex = 2;
            this.cSharpCheckBox3.Text = "cSharpCheckBox3";
            this.cSharpCheckBox3.UseVisualStyleBackColor = true;
            // 
            // CheckBoxDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpCheckBox3);
            this.Controls.Add(this.cSharpCheckBox2);
            this.Controls.Add(this.cSharpCheckBox1);
            this.Name = "CheckBoxDemo";
            this.Text = "CheckBoxDemo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CheckBox.CSharpCheckBox cSharpCheckBox1;
        private CheckBox.CSharpCheckBox cSharpCheckBox2;
        private CheckBox.CSharpCheckBox cSharpCheckBox3;
    }
}