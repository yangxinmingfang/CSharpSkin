﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.SplitContainer
{
    #region 点击收缩按钮时隐藏的Panel
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 点击收缩按钮时隐藏的Panel
    /// </summary>
    public enum CollapsePanel
    {
        None = 0,
        PanelOne = 1,
        PanelTwo = 2,
    }
    #endregion
}
