﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ContextMenuStrip
{
    #region 操作系统API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 操作系统API
    /// </summary>
    public class CSharpWinapi
    {
        /// <summary>
        /// 自左向右滚动窗体动画效果
        /// </summary>
        public const Int32 AW_HOR_POSITIVE = 0X00000001;

        /// <summary>
        /// 自右向左滚动窗体动画效果
        /// </summary>
        public const Int32 AW_HOR_NEGATIVE = 0X00000002;

        /// <summary>
        /// 自上向下滚动窗体动画效果
        /// </summary>
        public const Int32 AW_VER_POSITIVE = 0X00000004;

        /// <summary>
        /// 自下向上滚动窗体动画效果
        /// </summary>
        public const Int32 AW_VER_NEGATIVE = 0X00000008;
        public const Int32 AW_CENTER = 0X00000010;
        public const Int32 AW_HIDE = 0X00010000;
        public const Int32 AW_ACTIVATE = 0X00020000;
        public const Int32 AW_SLIDE = 0X00040000;
        public const Int32 AW_BLEND = 0X00080000;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool AnimateWindow(IntPtr hwnd, int dwTime, int dwFlags);

        #region 以动画形式显示窗体
        /// <summary>
        /// 以动画形式显示窗体
        /// </summary>
        /// <param name="hwnd"></param>
        public static void WindowsShow(IntPtr hwnd)
        {
            AnimateWindow(hwnd, 100, AW_SLIDE + AW_HOR_POSITIVE);
        }
        #endregion

        #region 动画参数说明
        //自左向右滚动窗体动画效果  AnimateWindow(this.Handle,2000,AW_HOR_POSITIVE);
        //"自左向右滑动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_SLIDE+AW_HOR_POSITIVE);
        //"自右向左滚动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_HOR_NEGATIVE);
        //"自右向左滑动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_SLIDE + AW_HOR_NEGATIVE);
        //"自上向下滚动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_VER_POSITIVE);
        //"自上向下滑动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_SLIDE + AW_VER_POSITIVE);
        //"自下向上滚动窗体动画效果"  AnimateWindow(this.Handle, 2000, AW_VER_NEGATIVE);
        //"自下向上滑动窗体动画效果" AnimateWindow(this.Handle, 2000, AW_SLIDE + AW_VER_NEGATIVE);
        //"向外扩展窗体动画效果") AnimateWindow(this.Handle, 2000, AW_SLIDE + AW_CENTER);
        //"淡入窗体动画效果") AnimateWindow(this.Handle, 2000, AW_BLEND);
        #endregion
    }
    #endregion
}
