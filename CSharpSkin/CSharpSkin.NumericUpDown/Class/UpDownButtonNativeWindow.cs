﻿using CSharpSkin.NumericUpDown.Event;
using CSharpSkin.NumericUpDown.Struct;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.NumericUpDown.Class
{
    #region 数字选择框扩展窗口
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 数字选择框扩展窗口
    /// </summary>
    public class NumericUpDownButtonNativeWindow : NativeWindow, IDisposable
    {

        private CSharpNumericUpDown _owner;
        private Control _upDownButton;
        private IntPtr _upDownButtonWnd;
        private bool _bPainting;
        private const int WM_PAINT = 0xF;
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="owner"></param>
        public NumericUpDownButtonNativeWindow(CSharpNumericUpDown owner) : base()
        {
            _owner = owner;
            _upDownButton = owner.NumericUpDownButton;
            _upDownButtonWnd = _upDownButton.Handle;
            base.AssignHandle(_upDownButtonWnd);
        }
        #endregion

        #region 是否按下鼠标左键
        /// <summary>
        /// 是否按下鼠标左键
        /// </summary>
        /// <returns></returns>
        private bool IsMouseLeftKeyPressed()
        {
            if (SystemInformation.MouseButtonsSwapped)
            {
                return (CSharpWinapi.GetKeyState(VK_RBUTTON) < 0);
            }
            else
            {
                return (CSharpWinapi.GetKeyState(VK_LBUTTON) < 0);
            }
        }
        #endregion

        #region 绘制按钮
        /// <summary>
        /// 绘制按钮
        /// </summary>
        private void DrawUpDownButton()
        {
            bool mouseOver = false;
            bool mousePress = IsMouseLeftKeyPressed();
            bool mouseInUpButton = false;
            Rectangle clipRect = _upDownButton.ClientRectangle;
            RECT windowRect = new RECT();
            Point cursorPoint = new Point();
            CSharpWinapi.GetCursorPos(ref cursorPoint);
            CSharpWinapi.GetWindowRect(_upDownButtonWnd, ref windowRect);
            mouseOver = CSharpWinapi.PtInRect(ref windowRect, cursorPoint);
            cursorPoint.X -= windowRect.Left;
            cursorPoint.Y -= windowRect.Top;
            mouseInUpButton = cursorPoint.Y < clipRect.Height / 2;
            using (Graphics g = Graphics.FromHwnd(_upDownButtonWnd))
            {
                NumericUpDownButtonPaintEventArgs e =new NumericUpDownButtonPaintEventArgs(g,clipRect,mouseOver,mousePress,mouseInUpButton);
                _owner.OnPaintNumericUpDownButton(e);
            }
        }
        #endregion

        #region 监听系统消息绘制
        /// <summary>
        /// 监听系统消息绘制
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_PAINT:
                    if (!_bPainting)
                    {
                        _bPainting = true;
                        PAINTSTRUCT ps = new PAINTSTRUCT();
                        CSharpWinapi.BeginPaint(m.HWnd, ref ps);
                        DrawUpDownButton();
                        CSharpWinapi.EndPaint(m.HWnd, ref ps);
                        _bPainting = false;
                        m.Result = WMRESULT.TRUE;
                    }
                    else
                    {
                        base.WndProc(ref m);
                    }
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion

        #region 释放
        /// <summary>
        /// 释放
        /// </summary>
        public void Dispose()
        {
            _owner = null;
            _upDownButton = null;
            base.ReleaseHandle();
        }
        #endregion
    }
    #endregion
}
