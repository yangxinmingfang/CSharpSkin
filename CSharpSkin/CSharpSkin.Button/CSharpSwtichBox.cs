﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.Button
{
    #region 切换卡图片按钮
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 切换卡图片按钮
    /// </summary>
    public class CSharpSwtichBox:System.Windows.Forms.PictureBox
    {
        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        public CSharpSwtichBox()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Transparent;
        }
        #endregion

        #region 定义事件，值改变时发生
        /// <summary>
        /// 定义事件，值改变时发生
        /// </summary>
        /// <param name="value"></param>
        public delegate void ValueChanged(int value);
        public event ValueChanged OnValueChanged;
        #endregion

        #region 默认图片
        /// <summary>
        /// 默认图片
        /// </summary>
        private Bitmap _defaultBitmap;
        [Browsable(true)]
        [Description("默认图片")]
        public Bitmap DefaultBitmap
        {
            get { return _defaultBitmap; }
            set
            {
                _defaultBitmap = value;
                if (_defaultBitmap != null)
                {
                    _defaultBitmap.MakeTransparent(Color.FromArgb(192, 192, 192));
                    Invalidate();
                }
            }
        }
        #endregion

        #region  切换后图片
        /// <summary>
        /// 切换后图片
        /// </summary>
        private Bitmap _switchImage;
        [Browsable(true)]
        [Description("切换后的图片")]
        public Bitmap SwitchImage
        {
            get { return _switchImage; }
            set
            {
                _switchImage = value;
                if (_switchImage != null)
                {
                    _switchImage.MakeTransparent(Color.FromArgb(192, 192, 192));
                    Invalidate();
                }
            }
        }
        #endregion

        #region 值
        /// <summary>
        /// 值
        /// </summary>
        private int _value = 0;
        [Browsable(true)]
        [Description("是否默认,0为默认 1为切换")]
        public int Value
        {
            get { return _value; }
            set
            {
                if (value > 1) value = 1;
                if (value < 1) value = 0;
                _value = value;
                if (OnValueChanged != null)
                {
                    OnValueChanged.Invoke(value);
                }
                Invalidate();
            }
        }
        #endregion

        #region 鼠标单击
        /// <summary>
        /// 鼠标单击
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (Value == 1)
            {
                Value = 0;
            }
            else
            {
                Value = 1;
            }
            Invalidate();
        }
        #endregion

        #region 控件尺寸改变触发事件
        /// <summary>
        /// 控件尺寸改变触发事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            Invalidate();
        }
        #endregion

        #region 绘制控件
        /// <summary>
        /// 绘制控件
        /// </summary>
        /// <param name="pe"></param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            if (this._switchImage != null && this._defaultBitmap != null)
            {
                if (Value == 1)
                {
                    pe.Graphics.DrawImage(this._switchImage, 0, 0, this.Width, this.Height);
                }
                else
                {
                    pe.Graphics.DrawImage(this._defaultBitmap, 0, 0, this.Width, this.Height);

                }
            }
            pe.Dispose();
        }
        #endregion
    }
    #endregion
}
