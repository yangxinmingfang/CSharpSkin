﻿using CSharpSkin.TabControl.Event;
using CSharpSkin.TabControl.Struct;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.TabControl.Class
{
    #region 绘制选项卡按钮窗口
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 绘制选项卡按钮窗口
    /// </summary>
    public class TabControlUpDownButtonNativeWindow:NativeWindow,IDisposable
    {
        private CSharpTabControl _owner;
        private bool _isPainting;
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        public const int WM_PAINT = 0xF;
        private const int TCM_FIRST = 0x1300;
        public const int TCM_GETITEMRECT = (TCM_FIRST + 10);

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="owner"></param>
        public TabControlUpDownButtonNativeWindow(CSharpTabControl owner): base()
        {
            _owner = owner;
            base.AssignHandle(owner.UpDownButtonHandle);
        }
        #endregion

        #region 判断鼠标是否按下左键
        /// <summary>
        /// 判断鼠标是否按下左键
        /// </summary>
        /// <returns></returns>
        private bool IsMouseLeftKeyPressed()
        {
            if (SystemInformation.MouseButtonsSwapped)
            {
                return (CSharpWinapi.GetKeyState(VK_RBUTTON) < 0);
            }
            else
            {
                return (CSharpWinapi.GetKeyState(VK_LBUTTON) < 0);
            }
        }
        #endregion

        #region 绘制按钮
        /// <summary>
        /// 绘制按钮
        /// </summary>
        private void DrawUpDownButton()
        {
            bool mouseOver = false;
            bool mousePress = IsMouseLeftKeyPressed();
            bool mouseInUpButton = false;
            RECT rect = new RECT();
            CSharpWinapi.GetClientRect(base.Handle, ref rect);
            Rectangle clipRect = Rectangle.FromLTRB(rect.Top, rect.Left, rect.Right, rect.Bottom);
            Point cursorPoint = new Point();
            CSharpWinapi.GetCursorPos(ref cursorPoint);
            CSharpWinapi.GetWindowRect(base.Handle, ref rect);
            mouseOver = CSharpWinapi.PtInRect(ref rect, cursorPoint);
            cursorPoint.X -= rect.Left;
            cursorPoint.Y -= rect.Top;
            mouseInUpButton = cursorPoint.X < clipRect.Width / 2;
            using (Graphics g = Graphics.FromHwnd(base.Handle))
            {
                TabControlUpDownButtonPaintEventArgs e =new TabControlUpDownButtonPaintEventArgs(g,clipRect, mouseOver,mousePress, mouseInUpButton);
                _owner.OnPaintTabControlUpDownButton(e);
            }
        }
        #endregion

        #region 监听系统消息进行重绘
        /// <summary>
        /// 监听系统消息进行重绘
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_PAINT:
                    if (!_isPainting)
                    {
                        PAINTSTRUCT ps =new PAINTSTRUCT();
                        _isPainting = true;
                        CSharpWinapi.BeginPaint(m.HWnd, ref ps);
                        DrawUpDownButton();
                        CSharpWinapi.EndPaint(m.HWnd, ref ps);
                        _isPainting = false;
                        m.Result = WMRESULT.TRUE;
                    }
                    else
                    {
                        base.WndProc(ref m);
                    }
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion

        #region 释放
        public void Dispose()
        {
            _owner = null;
            base.ReleaseHandle();
        }
        #endregion
    }
    #endregion
}
