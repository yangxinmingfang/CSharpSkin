﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CSharpSkin.TreeView
{
    #region 系统函数API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 系统函数API
    /// </summary>
    public class CSharpWinapi
    {
        #region  常量
        /// <summary>
        /// 常量
        /// </summary>
        private const int WS_HSCROLL = 0x100000;
        private const int WS_VSCROLL = 0x200000;
        private const int GWL_STYLE = (-16);
        public const int SB_HORZ = 0;
        public const int SB_VERT = 1;
        public const int SB_CTL = 2;
        public const int SB_BOTH = 3;
        public const int SW_SHOW = 1;
        public const int WM_PRINTCLIENT = 0x0318;
        public const int PRF_CLIENT = 0x00000004;
        #endregion

        #region 拖动文件
        /// <summary>
        /// 拖动文件
        /// </summary>
        /// <param name="hDrop"></param>
        /// <param name="iFile"></param>
        /// <param name="lpszFile"></param>
        /// <param name="cch"></param>
        /// <returns></returns>
        [DllImport("shell32.dll")]
        public static extern uint DragQueryFile(int hDrop, uint iFile, StringBuilder lpszFile, uint cch);
        #endregion

        #region 接收拖动文件
        /// <summary>
        /// 接收拖动文件
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="fAccept"></param>
        [DllImport("shell32.dll")]
        public static extern void DragAcceptFiles(IntPtr hWnd, bool fAccept);
        #endregion

        #region 拖动接收
        /// <summary>
        /// 拖动接收
        /// </summary>
        /// <param name="hDrop"></param>
        [DllImport("shell32.dll")]
        public static extern void DragFinish(int hDrop);
        const int WM_DROPFILES = 0x0233;
        #endregion

        #region 是否优化内存
        /// <summary>
        /// 是否优化内存
        /// </summary>
        /// <param name="process"></param>
        /// <param name="minSize"></param>
        /// <param name="maxSize"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize")]
        public static extern int SetProcessWorkingSetSize(IntPtr process, int minSize, int maxSize); ////// 释放内存 
        #endregion

        #region 将指定句柄的窗体设置靠最前显示
        /// <summary>
        /// 将指定句柄的窗体设置靠最前显示
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        #endregion

        #region 显示窗体
        /// <summary>
        /// 显示窗体
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="cmdShow"></param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int cmdShow);
        #endregion

        #region 加载鼠标光标
        /// <summary>
        /// 加载鼠标光标
        /// </summary>
        /// <param name="hInstance"></param>
        /// <param name="cursor"></param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern IntPtr LoadCursor(IntPtr hInstance, CSharpCursorType cursor);
        #endregion

        #region 获取窗体数据
        /// <summary>
        /// 获取窗体数据
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hwnd, int nIndex);
        #endregion

        #region 显示滚动条
        /// <summary>
        /// 显示滚动条
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="wBar"></param>
        /// <param name="bShow"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool ShowScrollBar(IntPtr hWnd, int wBar, bool bShow);
        #endregion

        #region 设置窗体样式
        /// <summary>
        /// 设置窗体样式
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="pszSubAppName"></param>
        /// <param name="pszSubIdList"></param>
        /// <returns></returns>
        [DllImport("uxtheme.dll", CharSet = CharSet.Unicode)]
        public extern static int SetWindowTheme(IntPtr hWnd, string pszSubAppName, string pszSubIdList);
        #endregion

        #region 返回鼠标Hand光标
        /// <summary>
        /// 返回鼠标Hand光标
        /// </summary>
        public static Cursor Hand
        {
            get
            {
                IntPtr h = LoadCursor(IntPtr.Zero, CSharpCursorType.IDC_HAND);
                return new Cursor(h);
            }
        }
        #endregion

        #region 判断是否出现垂直滚动条
        /// <summary>
        /// 判断是否出现垂直滚动条
        /// </summary>
        /// <param name="ctrl">待测控件</param>
        /// <returns>出现垂直滚动条返回true，否则为false</returns>
        public static bool IsVerticalScrollBarVisible(Control ctrl)
        {
            if (!ctrl.IsHandleCreated)
                return false;

            return (GetWindowLong(ctrl.Handle, GWL_STYLE) & WS_VSCROLL) != 0;
        }
        #endregion

        #region 判断是否出现水平滚动条
        /// <summary>
        /// 判断是否出现水平滚动条
        /// </summary>
        /// <param name="ctrl">待测控件</param>
        /// <returns>出现水平滚动条返回true，否则为false</returns>
        public static bool IsHorizontalScrollBarVisible(Control ctrl)
        {
            if (!ctrl.IsHandleCreated)
                return false;
            return (GetWindowLong(ctrl.Handle, GWL_STYLE) & WS_HSCROLL) != 0;
        }
        #endregion

        #region 向指定句柄发送消息
        /// <summary>
        /// 向指定句柄发送消息
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
        #endregion

        #region 返回当前系统是否是XP
        /// <summary>
        /// 返回当前系统是否是XP
        /// </summary>
        public static bool IsXPOS
        {
            get
            {
                OperatingSystem operatingSystem = Environment.OSVersion;
                return (operatingSystem.Platform == PlatformID.Win32NT) &&
                    ((operatingSystem.Version.Major > 5) || ((operatingSystem.Version.Major == 5) && (operatingSystem.Version.Minor == 1)));
            }
        }
        #endregion

        #region 返回当前系统是否是Visita
        /// <summary>
        /// 返回当前系统是否是Visita
        /// </summary>
        public static bool IsVistaOS
        {
            get
            {
                OperatingSystem operatingSystem = Environment.OSVersion;
                return (operatingSystem.Platform == PlatformID.Win32NT) && (operatingSystem.Version.Major >= 6);
            }
        }
        #endregion
    }
    #endregion
}
