﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using CSharpSkin.TrackBar.Struct;

namespace CSharpSkin.TrackBar
{
    #region 操作系统API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 操作系统API
    /// </summary>
    public class CSharpWinapi
    {
        #region 开始绘制
        /// <summary>
        /// 开始绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr BeginPaint(
           IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion

        #region 结束绘制
        /// <summary>
        /// 结束绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool EndPaint(
            IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion

        #region 创建一个具有扩展风格的重叠式窗口、弹出式窗口或子窗口
        /// <summary>
        /// 创建一个具有扩展风格的重叠式窗口、弹出式窗口或子窗口
        /// </summary>
        /// <param name="exstyle"></param>
        /// <param name="lpClassName"></param>
        /// <param name="lpWindowName"></param>
        /// <param name="dwStyle"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="nWidth"></param>
        /// <param name="nHeight"></param>
        /// <param name="hwndParent"></param>
        /// <param name="Menu"></param>
        /// <param name="hInstance"></param>
        /// <param name="lpParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true,
            CharSet = CharSet.Unicode, BestFitMapping = false)]
        public static extern IntPtr CreateWindowEx(
            int exstyle,
            string lpClassName,
            string lpWindowName,
            int dwStyle,
            int x,
            int y,
            int nWidth,
            int nHeight,
            IntPtr hwndParent,
            IntPtr Menu,
            IntPtr hInstance,
            IntPtr lpParam);
        #endregion

        #region 销毁窗口句柄
        /// <summary>
        /// 销毁窗口句柄
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyWindow(IntPtr hWnd);
        #endregion

        #region 加载指定文件图标
        /// <summary>
        /// 加载指定文件图标
        /// </summary>
        /// <param name="hInstance"></param>
        /// <param name="lpIconName"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr LoadIcon(
            IntPtr hInstance, int lpIconName);
        #endregion

        #region 清除图标和释放任何被图标占用的存储空间
        /// <summary>
        /// 清除图标和释放任何被图标占用的存储空间
        /// </summary>
        /// <param name="hIcon"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyIcon(IntPtr hIcon);
        #endregion

        #region 设置窗口位置
        /// <summary>
        /// 设置窗口位置
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="hWndAfter"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(
            IntPtr hWnd,
            IntPtr hWndAfter,
            int x,
            int y,
            int cx,
            int cy,
            uint flags);
        #endregion

        #region 获取指定窗口尺寸
        /// <summary>
        /// 获取客户端窗口尺寸
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetClientRect(
            IntPtr hWnd, ref RECT r);
        #endregion

        #region 获取窗口尺寸
        /// <summary>
        /// 获取窗口尺寸
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpRect"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(
            IntPtr hWnd, ref RECT lpRect);
        #endregion

        #region 获取改变坐标后的矩形
        /// <summary>
        /// 获取改变坐标后的矩形
        /// </summary>
        /// <param name="lpRect"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public extern static int OffsetRect(
            ref RECT lpRect, int x, int y);
        #endregion

        #region 获取窗口信息
        /// <summary>
        /// 获取窗口信息
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowLong(
            IntPtr hwnd, int nIndex);
        #endregion

        #region 设置窗口信息
        /// <summary>
        /// 设置窗口信息
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="nIndex"></param>
        /// <param name="dwNewLong"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(
            IntPtr hwnd, int nIndex, int dwNewLong);
        #endregion

        #region 获取当前鼠标位置
        /// <summary>
        /// 获取当前鼠标位置
        /// </summary>
        /// <param name="lpPoint"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetCursorPos(ref Point lpPoint);
        #endregion

        #region 获取窗口在屏幕的坐标
        /// <summary>
        /// 获取窗口在屏幕的坐标
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpPoint"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool ScreenToClient(IntPtr hWnd, ref Point lpPoint);
        #endregion

        #region 指定窗口的客户端区域或者整个屏幕从一个设备上下文(DC)中提取一个句柄
        /// <summary>
        /// 指定窗口的客户端区域或者整个屏幕从一个设备上下文(DC)中提取一个句柄
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetDC(IntPtr handle);
        #endregion

        #region 释放句柄
        /// <summary>
        /// 释放句柄
        /// </summary>
        /// <param name="handle">要释放的设备场景相关的窗口句柄</param>
        /// <param name="hdc">要释放的设备场景句柄</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int ReleaseDC(IntPtr handle, IntPtr hdc);
        #endregion

        #region 返回桌面窗口的句柄。桌面窗口覆盖整个屏幕。
        /// <summary>
        /// 返回桌面窗口的句柄。桌面窗口覆盖整个屏幕。
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = false)]
        public static extern IntPtr GetDesktopWindow();
        #endregion

        #region 当在指定时间内鼠标指针离开或盘旋在一个窗口上时，此函数寄送消息
        /// <summary>
        /// 当在指定时间内鼠标指针离开或盘旋在一个窗口上时，此函数寄送消息
        /// </summary>
        /// <param name="lpEventTrack"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool TrackMouseEvent(
            ref LPTRACKMOUSEEVENT lpEventTrack);
        #endregion

        #region 判断指定的点是否在矩形中
        /// <summary>
        /// 判断指定的点是否在矩形中
        /// </summary>
        /// <param name="lprc"></param>
        /// <param name="pt"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool PtInRect(ref RECT lprc, Point pt);
        #endregion

        #region 创建或设置一个定时器，该函数创建的定时器与Timer控件（定时器控件）效果相同。
        /// <summary>
        /// 创建或设置一个定时器，该函数创建的定时器与Timer控件（定时器控件）效果相同。
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="nIDEvent"></param>
        /// <param name="uElapse"></param>
        /// <param name="lpTimerFunc"></param>
        /// <returns></returns>
        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr SetTimer(
            IntPtr hWnd,
            int nIDEvent,
            uint uElapse,
            IntPtr lpTimerFunc);
        #endregion

        #region 销毁以前调用SetTimer创建的用nIDEvent标识的定时器事件。不能将此定时器有关的未处理的WM_TIMER消息都从消息队列中清除。
        /// <summary>
        /// 销毁以前调用SetTimer创建的用nIDEvent标识的定时器事件。不能将此定时器有关的未处理的WM_TIMER消息都从消息队列中清除。
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="uIDEvent"></param>
        /// <returns></returns>
        [DllImport("user32.dll", ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool KillTimer(
            IntPtr hWnd, uint uIDEvent);
        #endregion

        #region 对指定的窗口设置键盘焦点
        /// <summary>
        /// 对指定的窗口设置键盘焦点
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int SetFocus(IntPtr hWnd);
        #endregion

        #region 发送消息到指定句柄
        /// <summary>
        /// 发送消息到指定句柄
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, int wParam, ref TOOLINFO lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, int wParam, ref RECT lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd,
            int msg,
            IntPtr wParam,
            [MarshalAs(UnmanagedType.LPTStr)]string lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, IntPtr wParam, ref NMHDR lParam);

        [DllImport("user32.dll")]
        public extern static int SendMessage(
            IntPtr hWnd, int msg, IntPtr wParam, int lParam);
        #endregion

        #region 获取指定虚拟键的状态
        /// <summary>
        /// 获取指定虚拟键的状态
        /// </summary>
        /// <param name="nVirtKey"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern short GetKeyState(int nVirtKey);
        #endregion

        #region 更新指定窗口的无效矩形区域，使之有效
        /// <summary>
        /// 更新指定窗口的无效矩形区域，使之有效
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpRect"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);
        #endregion

        #region 用来显示具有指定透明度的图像。
        /// <summary>
        /// 用来显示具有指定透明度的图像。
        /// </summary>
        /// <param name="hdcDest"></param>
        /// <param name="nXOriginDest"></param>
        /// <param name="nYOriginDest"></param>
        /// <param name="nWidthDest"></param>
        /// <param name="nHeightDest"></param>
        /// <param name="hdcSrc"></param>
        /// <param name="nXOriginSrc"></param>
        /// <param name="nYOriginSrc"></param>
        /// <param name="nWidthSrc"></param>
        /// <param name="nHeightSrc"></param>
        /// <param name="blendFunction"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll", EntryPoint = "GdiAlphaBlend")]
        public static extern bool AlphaBlend(
            IntPtr hdcDest,
            int nXOriginDest,
            int nYOriginDest,
            int nWidthDest,
            int nHeightDest,
            IntPtr hdcSrc,
            int nXOriginSrc,
            int nYOriginSrc,
            int nWidthSrc,
            int nHeightSrc,
            BLENDFUNCTION blendFunction);
        #endregion

        #region 该函数从源矩形中复制一个位图到目标矩形，必要时按目标设备设置的模式进行图像的拉伸或压缩
        /// <summary>
        /// 该函数从源矩形中复制一个位图到目标矩形，必要时按目标设备设置的模式进行图像的拉伸或压缩
        /// </summary>
        /// <param name="hDest"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="nWidth"></param>
        /// <param name="nHeight"></param>
        /// <param name="hdcSrc"></param>
        /// <param name="sX"></param>
        /// <param name="sY"></param>
        /// <param name="nWidthSrc"></param>
        /// <param name="nHeightSrc"></param>
        /// <param name="dwRop"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool StretchBlt(
            IntPtr hDest,
            int X,
            int Y,
            int nWidth,
            int nHeight,
            IntPtr hdcSrc,
            int sX,
            int sY,
            int nWidthSrc,
            int nHeightSrc,
            int dwRop);
        #endregion

        #region 对指定的源设备环境区域中的像素进行位块（bit_block）转换
        /// <summary>
        /// 对指定的源设备环境区域中的像素进行位块（bit_block）转换
        /// </summary>
        /// <param name="hdc"></param>
        /// <param name="nXDest"></param>
        /// <param name="nYDest"></param>
        /// <param name="nWidth"></param>
        /// <param name="nHeight"></param>
        /// <param name="hdcSrc"></param>
        /// <param name="nXSrc"></param>
        /// <param name="nYSrc"></param>
        /// <param name="dwRop"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(
            IntPtr hdc,
            int nXDest,
            int nYDest,
            int nWidth,
            int nHeight,
            IntPtr hdcSrc,
            int nXSrc,
            int nYSrc,
            int dwRop);
        #endregion

        #region 创建设备
        /// <summary>
        /// 创建设备
        /// </summary>
        /// <param name="lpszDriver"></param>
        /// <param name="lpszDevice"></param>
        /// <param name="lpszOutput"></param>
        /// <param name="lpInitData"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateDCA(
            [MarshalAs(UnmanagedType.LPStr)]string lpszDriver,
            [MarshalAs(UnmanagedType.LPStr)]string lpszDevice,
            [MarshalAs(UnmanagedType.LPStr)]string lpszOutput,
            int lpInitData);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateDCW(
            [MarshalAs(UnmanagedType.LPWStr)]string lpszDriver,
            [MarshalAs(UnmanagedType.LPWStr)]string lpszDevice,
            [MarshalAs(UnmanagedType.LPWStr)]string lpszOutput,
            int lpInitData);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateDC(
            string lpszDriver,
            string lpszDevice,
            string lpszOutput,
            int lpInitData);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        #endregion

        #region 创建与指定的设备环境相关的设备兼容的位图。
        /// <summary>
        /// 创建与指定的设备环境相关的设备兼容的位图。
        /// </summary>
        /// <param name="hdc"></param>
        /// <param name="nWidth"></param>
        /// <param name="nHeight"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateCompatibleBitmap(
            IntPtr hdc, int nWidth, int nHeight);
        #endregion

        #region 删除设备
        /// <summary>
        /// 删除设备
        /// </summary>
        /// <param name="hdc"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteDC(IntPtr hdc);
        #endregion

        #region 选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象。
        /// <summary>
        /// 选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象。
        /// </summary>
        /// <param name="hdc"></param>
        /// <param name="hgdiobj"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll", ExactSpelling = true, PreserveSig = true)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        #endregion

        #region 删除对象，释放所有与该对象有关的系统资源，在对象被删除之后，指定的句柄也就失效了。
        /// <summary>
        /// 删除对象，释放所有与该对象有关的系统资源，在对象被删除之后，指定的句柄也就失效了。
        /// </summary>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(IntPtr hObject);

        #endregion

        #region 如果一个运行在 Windows XP 上的应用程序清单指定要使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，则需要 InitCommonControlsEx()。否则，将无法创建窗口。
        /// <summary>
        /// 如果一个运行在 Windows XP 上的应用程序清单指定要使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，则需要 InitCommonControlsEx()。否则，将无法创建窗口。
        /// </summary>
        /// <param name="iccex"></param>
        /// <returns></returns>
        [DllImport("comctl32.dll",
            CallingConvention = CallingConvention.StdCall)]
        public static extern bool InitCommonControlsEx(
            ref INITCOMMONCONTROLSEX iccex);
        #endregion

        #region 从指定内存中复制内存至另一内存里.简称:复制内存.
        /// <summary>
        /// 从指定内存中复制内存至另一内存里.简称:复制内存.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="source"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            ref NMHDR destination, IntPtr source, int length);

        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            ref NMTTDISPINFO destination, IntPtr source, int length);

        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            IntPtr destination, ref NMTTDISPINFO Source, int length);

        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            ref POINT destination, ref RECT Source, int length);

        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            ref NMTTCUSTOMDRAW destination, IntPtr Source, int length);

        [DllImport("kernel32.dll")]
        public extern static int RtlMoveMemory(
            ref NMCUSTOMDRAW destination, IntPtr Source, int length);
        #endregion
    }
    #endregion
}
