﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.TrackBar.Struct
{
    public struct LPTRACKMOUSEEVENT
    {
        public uint cbSize;
        public LPTRACKMOUSEEVENT_FLAGS dwFlags;
        public IntPtr hwndTrack;
        public uint dwHoverTime;
    }
}
