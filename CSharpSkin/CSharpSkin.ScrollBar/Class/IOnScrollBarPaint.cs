﻿using CSharpSkin.ScrollBar.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Class
{
    public interface IOnScrollBarPaint
    {
        void OnPaintScrollBarArrow(PaintScrollBarArrowEventArgs e);
        void OnPaintScrollBar(PaintScrollBarEventArgs e);
        void OnPaintScrollBarTrack(PaintScrollBarTrackEventArgs e);
    }
}
