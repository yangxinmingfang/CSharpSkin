﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Enum
{
    public enum ScrollBarMouseDownState
    {
        None = 0,
        TopArrow,
        BottomArrow,
        LeftArrow,
        RightArrow,
        Thumb,
        Track
    }
}
