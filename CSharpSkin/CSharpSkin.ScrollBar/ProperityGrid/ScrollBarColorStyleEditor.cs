﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.ScrollBar.ProperityGrid
{
    #region CSharpToolTip颜色样式编辑器
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// CSharpScrollBar颜色样式编辑器
    /// </summary>
    public partial class ScrollBarColorStyleEditor : Form
    {
         ScrollBarColorStyle _scrollBarColorStyle;

        #region 返回CSharpScrollBar颜色样式
        /// <summary>
        /// 返回CSharpScrollBar颜色样式
        /// </summary>
        public ScrollBarColorStyle ScrollBarColorStyle
        {
            get {
                return _scrollBarColorStyle;
            }
        }
        #endregion

        #region 无参构造
        /// <summary>
        /// 构造
        /// </summary>
        public ScrollBarColorStyleEditor()
        {
            InitializeComponent();
        }
        #endregion

        #region 有参构造
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="scrollBarColorStyle"></param>
        public ScrollBarColorStyleEditor(ScrollBarColorStyle scrollBarColorStyle)
        {
            _scrollBarColorStyle = scrollBarColorStyle;
            InitializeComponent();
            this.Init();
            this.InitEvent();
        }
        #endregion

        #region 初始化值
        /// <summary>
        /// 初始化值
        /// </summary>
        private void Init()
        {
            this.plBackHoverColor.BackColor = _scrollBarColorStyle.BackHoverColor;
            this.plBackNormalColor.BackColor = _scrollBarColorStyle.BackNormalColor;
            this.plBackPressedColor.BackColor = _scrollBarColorStyle.BackPressedColor;
            this.plBaseColor.BackColor = _scrollBarColorStyle.BaseColor;
            this.plBorderColor.BackColor = _scrollBarColorStyle.BorderColor;
            this.plTipForeColor.BackColor = _scrollBarColorStyle.ForeColor;
            this.plTitleForeColor.BackColor = _scrollBarColorStyle.InnerBorderColor;
        }
        #endregion

        #region 加载事件
        /// <summary>
        /// 加载事件
        /// </summary>
        public void InitEvent()
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel)
                {
                    ct.Click += ColorPannel_Click;
                }
            }
        }
        #endregion

        #region 选择颜色事件
        /// <summary>
        /// 选择颜色事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ColorPannel_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Panel)sender).BackColor = colorDialog.Color;
            }
        }
        #endregion

        #region 确定
        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            this._scrollBarColorStyle.BackHoverColor = this.plBackHoverColor.BackColor;
            this._scrollBarColorStyle.BackNormalColor =this.plBackNormalColor.BackColor;
            this._scrollBarColorStyle.BackPressedColor = this.plBackPressedColor.BackColor;
            this._scrollBarColorStyle.BaseColor = this.plBaseColor.BackColor;
            this._scrollBarColorStyle.BorderColor =this.plBorderColor.BackColor;
            this._scrollBarColorStyle.ForeColor =this.plTipForeColor.BackColor;
            this._scrollBarColorStyle.InnerBorderColor = this.plTitleForeColor.BackColor;
            DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
    #endregion
}
