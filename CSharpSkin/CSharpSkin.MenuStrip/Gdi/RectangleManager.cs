﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.MenuStrip.Gdi
{
    # region 矩形区域管理
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 矩形区域管理
    /// </summary>
    public class RectangleManager
    {
        #region 根据内边距修改矩形区域
        /// <summary>
        /// 根据内边距修改矩形区域
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="padding"></param>
        /// <returns></returns>
        public static Rectangle PaddingRect(Rectangle rect, Padding padding)
        {
            rect.X += padding.Left;
            rect.Y += padding.Top;
            rect.Width -= padding.Horizontal;
            rect.Height -= padding.Vertical;
            return rect;
        }
        #endregion

        #region 转化矩形矩形区域X坐标
        /// <summary>
        /// 转化矩形矩形区域X坐标
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="withinBounds"></param>
        /// <returns></returns>
        public static Rectangle TranslatePositionX(Rectangle bounds, Rectangle withinBounds)
        {
            bounds.X = withinBounds.Width - bounds.Right;
            return bounds;
        }
        #endregion

        #region 判断是否是空矩形区域对象
        /// <summary>
        /// 判断是否是空矩形区域对象
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static bool IsEmptyRect(Rectangle rect)
        {
            return rect.Width <= 0 || rect.Height <= 0;
        }
        #endregion
    }
    #endregion
}
