﻿using CSharpSkin.MenuStrip.ProperityGrid;
using CSharpSkin.MenuStrip.Render;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.MenuStrip
{
    #region 菜单栏，工具栏，状态栏渲染组件
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 菜单栏，工具栏，状态栏渲染组件
    /// </summary>
    public partial class CSharpStripRenderComponent : Component
    {
        #region Strip渲染风格
        /// <summary>
        /// Strip渲染风格
        /// </summary>
        private CSharpStripRenderer _csharpStripRenderer  { get; set; }=new CSharpStripRenderer();
        #endregion


        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="container"></param>
        public CSharpStripRenderComponent(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
            ToolStripManager.Renderer = _csharpStripRenderer;
            _stripColorStyle.OnColorChangedEventHanlder += _stripColorStyle_OnColorChangedEventHanlder;
        }
        #endregion

        #region Strip颜色风格
        private StripColorStyle _stripColorStyle = new StripColorStyle();
        /// <summary>
        /// Strip颜色风格
        /// </summary>
        [Browsable(true)]
        [Description("Strip颜色风格")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor(typeof(StripColorStylePropertyEditor),typeof(UITypeEditor))]
        public StripColorStyle StripColorStyle
        {
            get
            {
                return _stripColorStyle;
            }
            set
            {
                _stripColorStyle = value;
               
            }
      
        }

        private void _stripColorStyle_OnColorChangedEventHanlder()
        {
            _csharpStripRenderer = null;
            //ToolStripManager.Renderer = null;
            _csharpStripRenderer = new CSharpStripRenderer();
            _csharpStripRenderer.StripColorStyle = _stripColorStyle;
            ToolStripManager.Renderer = _csharpStripRenderer;
        }
        #endregion
    }
    #endregion
}
